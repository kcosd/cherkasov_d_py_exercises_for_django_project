from django.urls import path
from . import views


app_name = 'meal_plans'
urlpatterns = [
    # Домашняя страница
    path('', views.meal_plans_f, name='index'),
]