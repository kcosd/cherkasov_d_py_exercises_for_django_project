from django.shortcuts import render


def meal_plans_f(request):
    return render(request, 'meal_plans/index.html')
