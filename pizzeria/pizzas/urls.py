from django.urls import path
from . import views


app_name = 'pizzas'
urlpatterns = [
    # Домашняя страница с видами пиццы
    path('', views.index, name='index'),
    # Страница с добавками для пиццы
    path('<int:pizza_id>/', views.pizza_toping, name='pizza_toping'),

]