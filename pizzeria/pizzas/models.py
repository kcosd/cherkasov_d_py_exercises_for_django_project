from django.db import models

class Pizza(models.Model):
    """таблица видов пиццы"""
    name = models.CharField(max_length=100)  # название пиццы

    def __str__(self):  # описание таблицы
        return self.name


class Topping(models.Model):
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)  # название добавки

    def __str__(self):  # описание таблицы
        return self.name
