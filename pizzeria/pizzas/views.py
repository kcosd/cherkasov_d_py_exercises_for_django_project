from django.shortcuts import render
from .models import Pizza, Topping

def index(request):
    pizza_types_info = Pizza.objects.all()
    pizza_types_context = {'pizzas': pizza_types_info}
    return render(request, 'pizzas/main_pizzas.html', pizza_types_context)

def pizza_toping(request, pizza_id):
    pizza = Pizza.objects.get(id=pizza_id)
    toppings = pizza.topping_set.all()

    context = {'pizza': pizza, 'toppings': toppings}
    return render(request, 'pizzas/pizza_toping.html', context)






